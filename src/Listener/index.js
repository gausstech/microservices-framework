const express = require('express');
const app = express();

require('dotenv').config();

app.listen(process.env.PORT || 8080);

const bodyParser = require('body-parser');
const cors = require('cors');

app.use( bodyParser.json() );
app.use( cors() );

module.exports = function(schema, callback){
    schemaValidator(schema);
    app.all(schema.path, (req, res) => {
        if(schema.method){
            if(schema.method == req.method){
                callback(req, res);
            }
            else{
                res.send("cannot "+req.method);
            }
        }
        else{
            callback(req, res);
        }
    });
}

function schemaValidator(schema){
    if(!schema.path){
        throw "no path specified";
    }
}