module.exports = function(schema, tokendata, req, res){

    if(!schema.execute){
        throw "no execution function defined";
    }

    if(schema.getTokenData){
        if(tokendata){
            schema.execute(tokendata, req, res);
        }
        else{
            schema.execute(tokendata, req, res);
        }
    }
    else{
        schema.execute(req, res);
    }
}