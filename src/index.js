const listener = require('./Listener/index');
const auth = require('./Authentication/index');
const schemaFunction = require('./Schema/index');
const execute = require('./Execution/index');

module.exports.listen = function(schema){
    listener(schema, (req, res) => {
        auth(schema, req, res, (token) => {
            if(typeof(token) == "Boolean"){
                schemaFunction(schema, req, res, (result) => {
                    execute(schema, null, req, res);
                });
            }
            else{
                schemaFunction(schema, req, res, (result) => {
                    execute(schema, token, req, res);
                });
            }
        })
    })
}