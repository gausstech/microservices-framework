const jwt = require('jsonwebtoken');
require('dotenv').config();

let secret = process.env.SECRET;

module.exports = function(schema, req, res, callback){
    if(!schema.auth){
        if(req.headers['authentication']){
            let token = (req.headers['authentication']).split("Bearer ")[1];
            if(secret){
                let v = validate(String(token));
                if(v){
                    callback(v);
                }
                else{
                    res.json({
                        status: false,
                        err: {
                            msg: "authentication failed"
                        }
                    })
                }
            }
            else{
                callback(true)
            }
        }
        else{
            callback(true);
        }
    }
    else{
        if(!req.headers['authentication']){
            res.json({
                status: false,
                err: {
                    msg: "authentication failed"
                }
            })
        }
        else{
            let token = (req.headers['authentication']).split("Bearer ")[1];
            let v = validate(String(token));
            if(v){
                callback(v);
            }
            else{
                res.json({
                    status: false,
                    err: {
                        msg: "authentication failed"
                    }
                })
            }
        }
    }
}

function validate(token){
    if(!process.env.SECRET){
        throw "no secret specified";
    }

    try{
        return jwt.verify(token, secret);
    }
    catch(err){
        return false
    }
}

