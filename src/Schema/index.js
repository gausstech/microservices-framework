const SchemaObject = require('schema-object');

module.exports = function(schema, req, res, callback){
    if(schema.schema){
        let object = new SchemaObject(schema.schema);
        let otpobject = new object(req.body);
        if(otpobject.isErrors()){
            res.json({
                status: true,
                error: otpobject.getErrors()
            });
        }
        else{
            callback(req.body);
        }
    }
    else{
        callback(req.body);
    }
}