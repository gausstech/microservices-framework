# api-framework

## Functionallity

```js
    listen({
        path: "/path/to/listen",                //NOT OPTIONAL
        method: 'POST',                         //OPTIONAL
        schema: {                               //OPTIONAL
            name: String,
            age: Number
        },
        auth: true,                             //OPTIONAL
        tokenSecret: "secret"                   //OPTIONAL if not specified, uses the secret from "process.env.SECRET"
        getTokenData: true,                     //OPTIONAL
        execute: function(dec, req, res){       //NOT OPTIONAL returns error if not defined

        }
    });
```

### path
NOT OPTIONAL
Path is the path on which the server listens

### method
OPTIONAL if not sepecified, listens on all
Method is the http request method, the server listens for

### schema
OPTIONAL
Schema is the schema of the request body.
It uses [schema-object](https://github.com/scotthovestadt/schema-object)

### auth
OPTIONAL
Auth tells the api if a jwt token is used for authentication or not.
It only tells if a token is required.
Defaults to false if not declared.
Automatically uses the password under "SECRET" from the .env file

### tokenSecret
OPTIONAL
Specifies the secret of the token.
This can either be a string containing the secret or an object containing the algorithm method and the publickey.

### getTokenData
OPTIONAL
Defaults to true if not specified
Tells the api if it should return the decoded jwt-Token

### execute
NOT OPTIONAL
The method that gets executed when a request is successfull.
```js
{
    execute: function(dec, req, res){
        res.send("hello world");
    }
}
```
