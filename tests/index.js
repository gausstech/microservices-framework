const app = require('../src/index');

app.listen({
    path: "/test",
    method: 'POST',
    auth: false,
    schema: {
        name: {type: String, required: true}
    },
    getTokenData: true,
    execute: function(dec, req, res){
        res.send(dec);
        console.log(req.body)
    }
});
