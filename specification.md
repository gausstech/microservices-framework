```js
    listen({
        path: "/path/to/listen",                //NOT OPTIONAL
        method: 'POST',                         //OPTIONAL
        schema: {                               //OPTIONAL
            name: String,
            age: Number
        },
        auth: true,                             //OPTIONAL
        tokenSecret: "secret"                   //OPTIONAL if not specified, uses the secret from "process.env.SECRET"
        getTokenData: true,                     //OPTIONAL
        execute: function(dec, req, res){       //NOT OPTIONAL returns error if not defined

        }
    });
```

### path
NOT OPTIONAL
Path is the path on which the server listens

### method
OPTIONAL if not sepecified, listens on all
Method is the http request method, the server listens for

### schema
OPTIONAL
Schema is the schema of the request body.
It uses [schema-object](https://github.com/scotthovestadt/schema-object)

### auth
OPTIONAL
Auth tells the api if a jwt token is used for authentication or not.
It only tells if a token is required.
Defaults to false if not declared.
Automatically uses the password under "SECRET" from the .env file

### tokenSecret
OPTIONAL
Specifies the secret of the token.
This can either be a string containing the secret or an object containing the algorithm method and the publickey.

### getTokenData
OPTIONAL
Defaults to true if not specified
Tells the api if it should return the decoded jwt-Token

### execute
NOT OPTIONAL
The method that gets executed when a request is successfull.
```js
{
    execute: function(dec, req, res){
        res.send("hello world");
    }
}
```

## How it works

The api-framework is built out of multiple layers each of which executes in the sequence listed below and only jumps forward if the layer before is fully valid.

- Listener

The listener-layer listens to the incoming http requests.
It can be modified with the parameters path and method.

- Authentication

The authentication-layer parses the authentication header for valid a valid jwt token.
It can be modified through the parameters auth, tokenSecret and getTokenData.

- Schema

The schema-layer validates if every parameter in the post body is the right data type, exists and (if specified) looks if it has the right length, regex, etc. (all capabilities the schema-object module has).

- Execution

The execution-layer executes the code defined under the execution object.

# Microservices communication

The microservices-framework has a built in api for communication between your microservices.
It uses http(s) (right now only http is supported) to communicate. 
To sync all services you will have to create an account at [gcloud](https://gaussweb.net) and register your project.
There you will get a 32 digit token which you should store under GC_TOKEN as environment variable in all your microservices together with the name of the microservice under NAME e.g. user-mgmt, image-loader. To disable this functionality set the environment variable GC_ACTIVE to false.
After the microservice starts, it will send a hello message (POST) to https://gcloud.api.gaussweb.net/hello. 
From there it will get a 128 digit unique token which gets stored temporarily.




